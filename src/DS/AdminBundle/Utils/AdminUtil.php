<?php


namespace DS\AdminBundle\Utils;

class AdminUtil
{

    const ADMINISTRATOR_ROLE_NAME = 'Administrator';

    /**
     * @param <UserRoleAssignment> $roles
     */
    public static function isAdministrator($roles)
    {
        foreach ($roles as $role) {
            if ($role->role->identifier === self::ADMINISTRATOR_ROLE_NAME) {
                return true;
            }
        }
        return false;
    }
}