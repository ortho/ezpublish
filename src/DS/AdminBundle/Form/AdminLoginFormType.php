<?php

namespace DS\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AdminLoginFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', [
                'label' => 'Username',
                'required' => true,
                'trim' => true,
                'error_bubbling' => true,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('password', 'password', [
                'label' => 'Password',
                'required' => true,
                'trim' => true,
                'invalid_message' => 'The password fields must match.',
                'error_bubbling' => true,
                'attr' => [
                    'class' => 'form-control'
                ]
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'DS\AdminBundle\Entity\Admin'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'userlogin';
    }
}