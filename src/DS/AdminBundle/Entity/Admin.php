<?php

namespace DS\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity
 */
class Admin extends BaseUser
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }
}
