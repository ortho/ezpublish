<?php


namespace xsmtpdk\Bundle\ValueObjectVisitor;

use eZ\Publish\Core\REST\Common\Output\Generator;
use eZ\Publish\Core\REST\Common\Output\ValueObjectVisitor;
use eZ\Publish\Core\REST\Common\Output\Visitor;

//////////////////////////////////////////
class xWrapper extends ValueObjectVisitor
{
    public function visit(Visitor $visitor, Generator $generator, $data)
    {
        $generator->startObjectElement('xWrapper');
        $generator->startValueElement('ReqName', $data->reqname);
        $generator->endValueElement('ReqName');
        $generator->startValueElement('ResponseValue', $data->responsevalue);
        $generator->endValueElement('ResponseValue');
        $generator->endObjectElement('xWrapper');
    }
}
