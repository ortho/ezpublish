<?php


namespace xsmtpdk\Bundle\ValueObjectVisitor;

use eZ\Publish\Core\REST\Common\Output\Generator;
use eZ\Publish\Core\REST\Common\Output\ValueObjectVisitor;
use eZ\Publish\Core\REST\Common\Output\Visitor;

class Hello extends ValueObjectVisitor
{
    public function visit(Visitor $visitor, Generator $generator, $data)
    {
        $generator->startObjectElement('Test');
        $generator->startValueElement('Hello', $data->name);
        $generator->endValueElement('Hello');
        $generator->startValueElement('caca', strtoupper($data->name));
        $generator->endValueElement('caca');
        $generator->endObjectElement('Test');
    }
}
