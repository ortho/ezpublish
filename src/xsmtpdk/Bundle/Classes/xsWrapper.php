<?php
namespace xsmtpdk\Bundle\Classes;

class xsWrapper
{
    public $reqname;
    public $responsevalue;
    public $command;
    const YES = 'yes';
    const NO = 'no';

    public function __construct($command, $reqname, $responsevalue)
    {
        $this->command = $command;
        $this->reqname = $reqname;
        $this->responsevalue = $responsevalue;
    }

    //Static function to be called without instantiating an xWrapper
    public static function assemble($command, $reqname, $responsevalue)
    {
        return new xsWrapper($command, $reqname, $responsevalue);
    }

    //Returns a JSON encoded value
    public function toJson()
    {
        //\Doctrine\Common\Util\Debug::dump($this);
        return json_encode($this->toArray(), JSON_FORCE_OBJECT);
    }

    //Returns an Array value
    public function toArray()
    {
        $response = array(
            'command' => $this->command,
            'reqname' => $this->reqname,
            //'responsevalue'	=> $this->json_encode_objs($this->responsevalue),
        );
        if (is_object($this->responsevalue)) {
            $response['responsevalue'] = $this->json_encode_objs($this->responsevalue);
        } else {
            $response['responsevalue'] = $this->responsevalue;
        }
        //\Doctrine\Common\Util\Debug::dump($this);
        return $response;
    }

    //Traverses an object to array
    function json_encode_objs($item)
    {
        if (!is_array($item) && !is_object($item)) {
            return json_encode($item);
        } else {
            $pieces = array();
            foreach ($item as $k => $v) {

                $pieces[] = "\"$k\":" . json_encode_objs($v);
            }
            return '{' . implode(',', $pieces) . '}';
        }
    }
}
