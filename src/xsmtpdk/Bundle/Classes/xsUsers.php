<?php

namespace xsmtpdk\Bundle\Classes;

//Search includes;
use DateTime;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;

//For setField values

//
class xsUsers
{
    public $contentTypeService;
    public $contentService;
    public $logger;
    public $repository;
    public $searchService;
    public $locationService;
    public $urlAliasService;
    public $userService;

    public $ACC_EN_URL = "/Users/accounts_english/";
    public $ACC_DK_URL = "/Users/accounts_danish/";
    public $accounts_english_node;
    public $accounts_danish_node;
    public $accounts_english_cId;
    public $accounts_danish_cId;


    public function __construct(&$repository, &$logger, &$controller)
    {
        $this->repository = $repository;
        $this->logger = $logger;
        $this->controller = $controller;

        //Instantiate services;
        $this->contentTypeService = $repository->getContentTypeService();
        $this->contentService = $repository->getContentService();
        $this->locationService = $repository->getLocationService();
        $this->searchService = $repository->getSearchService();
        $this->urlAliasService = $repository->getURLAliasService();
        $this->userService = $repository->getUserService();

        //Get all user accounts folder node and content IDs
        $this->accounts_english_node = $this->urlAliasService->lookup($this->ACC_EN_URL)->destination;
        $this->accounts_danish_node = $this->urlAliasService->lookup($this->ACC_DK_URL)->destination;
        $this->accounts_english_cId = $this->locationService->loadLocation($this->accounts_english_node)->contentInfo->id;
        $this->accounts_danish_cId = $this->locationService->loadLocation($this->accounts_danish_node)->contentInfo->id;

    }

    ////////////////////////////////////////////////////////////
    public function userExists($login)
    {

        /*NOT RELIABLE FULL TEXT SEARCH WAY
        $query = new Query;
        //$query->criterion = new Criterion\ContentTypeIdentifier( array( "user" ) );
        $query->criterion = new Criterion\LogicalAnd(
            array(
                new Criterion\ContentTypeIdentifier( array("xs_account" ) ),
                new Criterion\Field( 'user', Criterion\Operator::EQ, $login ),
                //new Criterion\Status( array( Criterion\Status::STATUS_PUBLISHED ) )
                new Criterion\FullText( $login ),
            )
        );
        //$query->limit = 1;  // we only need to find one user with this email

        $searchResult = $this->searchService->findContent($query);
        foreach ($searchResult->searchHits as $elem){
            $user_account = $elem->valueObject->getFieldValue('user');
            if ($user_account->login == $login){  //check the login field only, but a search for email could be done here also
                //\Doctrine\Common\Util\Debug::dump($elem->valueObject->getFieldValue('user'));
                return true; //found the user
            }
        }
        return false; //user not found
        */
        try {
            // Throws exception since creation of user was rolled back
            $loadedUser = $this->userService->loadUserByLogin($login);
        } catch (\eZ\Publish\API\Repository\Exceptions\NotFoundException $e) {
            return false;
        }
        return true;

    }
    ////////////////////////////////////////////////////////////
    /*Sample $xs_account_data test data array*/
    /*
    $rnd = rand();
        $xs_account_data=array(
            'email' 	    => 'cucu'.$rnd.'@example.com',
            'login' 	    => 'cucu'.$rnd.'@example.com',
            'password' 	    => 'tester123',
            'first_name'	=> 'mohamad',
            'last_name'	    => 'ali',
            'postal_code'	=> '061234',
            'address'       => 'Emirates Holidays Building,
                                Sheikh Zayed Road
                                P.O Box 7631',
            'city'		    => 'Deva',
            'company'       => 'Boston Dynamics',
            'country'       => 'United Arab Emirates',
            'creation_date' => '2014-07-31 16:25:38',
            'telephone'     => '+971 4 214 4888',
            'lang'          => 'eng-GB',
        );
    */

    public function createUser($xs_account_data = array())
    {

        /* GOD MODE ON */
        $adminUser = $this->userService->loadUserByCredentials("admin", "publish");
        $this->repository->setCurrentUser($adminUser);
        $ezCountry = $this->controller->get("ezpublish.fieldtype.ezcountry");

        $toReturn = array();
        //Get accounts folder node and content IDs based on language
        switch ($xs_account_data['lang']) {
            case "eng-GB":
                $accounts_node = $this->accounts_english_node;
                $accounts_cId = $this->accounts_english_cId;
                break;

            case "dan-DK":
                $accounts_node = $this->accounts_danish_node;
                $accounts_cId = $this->accounts_danish_cId;
                break;
            default:
                $accounts_node = $this->accounts_english_node;
                $accounts_cId = $this->accounts_english_cId;
        }
        //This creates a new user of xs_account class (provided that the default user class is xs_account set in ez)
        // Instantiate a create struct with mandatory properties
        $userCreate = $this->userService->newUserCreateStruct(
            $xs_account_data['login'],
            $xs_account_data['email'],
            $xs_account_data['password'],
            'eng-GB'
        );
        $userCreate->enabled = true;

        // Set some fields required by the user ContentType
        $userCreate->setField('first_name', $xs_account_data['first_name']);
        $userCreate->setField('last_name', $xs_account_data['last_name']);

        // Load parent group for the user
        $group = $this->userService->loadUserGroup($accounts_cId);
        // Actually create the user
        $user = $this->userService->createUser($userCreate, array($group));

        //Get the newly created user's content ID
        $user_cId = $user->content->getVersionInfo()->getContentInfo()->id;
        $toReturn['user_object_id'] = $user_cId;
        //XPL_Debug
        $this->logger->debug(__METHOD__ . "\n" . "New user ${xs_account_data['login']} cId: ${user_cId}, created as ${xs_account_data['lang']} in user group node: ${accounts_node}");
        $this->logger->debug(__METHOD__ . "\n" . "New user (cId: ${user_cId}) input data: " . print_r($xs_account_data, true));
        //\\//

        //get the contentinfo
        $contentInfo = $user->content->getVersionInfo()->getContentInfo();
        //create a new draft of the object
        $contentDraft = $this->contentService->createContentDraft($contentInfo);
        //create a new update struct
        $contentUpdate = $this->contentService->newContentUpdateStruct();
        //commence setting the needed fields (that were not added during createUser)
        if (!isset($xs_account_data['initial_admin_email'])) $xs_account_data['initial_admin_email'] = $xs_account_data['email'];
        foreach ($xs_account_data as $key => $value)
            if (!in_array($key, array('login', 'password', 'email', 'lang'))) {
                switch ($key) {
                    case 'creation_date':
                        $contentUpdate->setField($key, new DateTime($value));
                        break;
                    case 'country':
                        $contentUpdate->setField($key, $ezCountry->fromHash(array($value)));
                        break;
                    default:
                        $contentUpdate->setField($key, $value);
                }

            }
        //update the draft
        $this->contentService->updateContent($contentDraft->getVersionInfo(), $contentUpdate);

        //Publish the content using the current version
        $pubContent = $this->contentService->publishVersion($contentDraft->getVersionInfo());

        //Get the contentInfo object from the published object (could have skipped this as I already have it)
        $pubContentInfo = $pubContent->getVersionInfo()->getContentInfo();
        //Get the location, from the locations Array.
        $newLocation = $this->locationService->loadLocations($pubContentInfo)[0];
        //Get the newly created node ID
        $nodeID = $newLocation->id;
        $toReturn['user_node_id'] = $nodeID;

        ///Creating containers (Extensions, Payments, Subscriptions) ( $contentCreate_container is recycled )
        $contentType_container = $this->contentTypeService->loadContentTypeByIdentifier('xs_account_extension_container');
        $contentCreate_container = $this->contentService->newContentCreateStruct($contentType_container, 'eng-GB');
        $contentCreate_container->setField('name', 'Extensions');
        $content_container = $this->contentService->createContent($contentCreate_container, array($this->locationService->newLocationCreateStruct($nodeID)));
        $pubContent_container = $this->contentService->publishVersion($content_container->getVersionInfo());
        $pubContent_container_info = $pubContent_container->getVersionInfo()->getContentInfo();
        $toReturn['extensions_object_id'] = $pubContent_container_info->id;
        $toReturn['extensions_node_id'] = $this->locationService->loadLocations($pubContent_container_info)[0]->id;

        $contentCreate_container->setField('name', 'Payments');
        $content_container = $this->contentService->createContent($contentCreate_container, array($this->locationService->newLocationCreateStruct($nodeID)));
        $pubContent_container = $this->contentService->publishVersion($content_container->getVersionInfo());
        $pubContent_container_info = $pubContent_container->getVersionInfo()->getContentInfo();
        $toReturn['payments_object_id'] = $pubContent_container_info->id;
        $toReturn['payments_node_id'] = $this->locationService->loadLocations($pubContent_container_info)[0]->id;

        $contentCreate_container->setField('name', 'Subscriptions');
        $content_container = $this->contentService->createContent($contentCreate_container, array($this->locationService->newLocationCreateStruct($nodeID)));
        $pubContent_container = $this->contentService->publishVersion($content_container->getVersionInfo());
        $pubContent_container_info = $pubContent_container->getVersionInfo()->getContentInfo();
        $toReturn['subscriptions_object_id'] = $pubContent_container_info->id;
        $toReturn['subscriptions_node_id'] = $this->locationService->loadLocations($pubContent_container_info)[0]->id;


        ///Crappy way Direct User value that does not create a real user!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //Create the content type from scratch
        /*
            $contentType = $this->contentTypeService->loadContentTypeByIdentifier( 'xs_account' );


            $contentCreate = $this->contentService->newContentCreateStruct( $contentType, 'eng-GB' );
            $contentCreate->setField( 'first_name', $xs_account_data['first_name'] );

            //$contentCreate->remoteId = 'abcdef0123456789abcdef0123456789';
            $contentCreate->alwaysAvailable = true;


            //set the user field
            $contentCreate->setField( 'user', $userValue );

            //Create the content object
            $content = $this->contentService->createContent( $contentCreate, array( $this->locationService->newLocationCreateStruct( $this->accounts_english_node ) ) );

        $this->logger->debug(__METHOD__."\n"."New content object: ".print_r($contentCreate, true));
        */


        //Updating the user datatype inside, after it was created.
        /*
        $raw_user = $this->userService->loadUser($content->getVersionInfo()->getContentInfo()->id);
        $raw_userUpdate = $this->userService->newUserUpdateStruct();
         // Set new values for password and maxLogin
        //$raw_userUpdate->login = $xs_account_data['login'];
        $raw_userUpdate->email = $xs_account_data['email'];
        $raw_userUpdate->password = $xs_account_data['password'];
        $raw_userUpdate->enabled = true;
        $this->userService->updateUser( $raw_user, $raw_userUpdate );
        $this->logger->debug(__METHOD__."\n"."New raw_user: ".print_r($raw_user, true));
        */


        //$this->logger->debug(__METHOD__."\n"."Stuff: ".print_r($user, true));
        /*
        //delete the userContentObject (but keeping the ezuser)
	    $stdUserContentInfo = $this->contentService->loadContentInfo($userValue->contentId);
	    $this->contentService->deleteContent($stdUserContentInfo);
	    */
        ////////
        //END CRAPPY WAY

        return $toReturn;
    }
    //

    /**
     * Get current user
     *
     * @return an array with email and login of the logged in user.
     */

    public function getCurrentUserLogin($xs_account_data = array())
    {

        $currentUser = $this->repository->getCurrentUser();
        if ($currentUser->login != "anonymous") {
            return array("login" => $currentUser->login, "email" => $currentUser->email);
        } else return array();

    }

    /////////////////////////
    public function tester()
    {
        return true;
    }

}
