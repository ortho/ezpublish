<?php

namespace xsmtpdk\Bundle\Classes;


// based on https://github.com/claviska/simple-php-captcha

class xsCaptcha
{
    public $logger;
    public $session;

    private $bg_path;
    private $font_path;
    private $captcha_config;

    public function __construct(&$session, &$logger, $config = array())
    {
        // Check for GD library
        if (!function_exists('gd_info')) {
            throw new Exception('Required GD library is missing');
        }

        //Initialise internal variables
        $this->logger = $logger;
        $this->session = $session;

        $this->bg_path = dirname(__FILE__) . '/simple-php-captcha/backgrounds/';
        $this->font_path = dirname(__FILE__) . '/simple-php-captcha/fonts/';

        // Default values
        $this->captcha_config = array(
            'code' => '',
            'min_length' => 6,
            'max_length' => 6,
            'backgrounds' => array(
                $this->bg_path . '45-degree-fabric.png',
                $this->bg_path . 'cloth-alike.png',
                $this->bg_path . 'grey-sandbag.png',
                $this->bg_path . 'kinda-jean.png',
                $this->bg_path . 'polyester-lite.png',
                $this->bg_path . 'stitched-wool.png',
                $this->bg_path . 'white-carbon.png',
                $this->bg_path . 'white-wave.png'
            ),
            'fonts' => array(
                $this->font_path . 'times_new_yorker.ttf'
            ),
            'characters' => 'ABCDEFGHJKLMNPRSTUVWXYZabcdefghjkmnprstuvwxyz23456789',
            'min_font_size' => 28,
            'max_font_size' => 28,
            'color' => '#666',
            'angle_min' => 0,
            'angle_max' => 10,
            'shadow' => true,
            'shadow_color' => '#fff',
            'shadow_offset_x' => -1,
            'shadow_offset_y' => 1
        );

        // Overwrite defaults with custom config values
        if (is_array($config)) {
            foreach ($config as $key => $value) $this->captcha_config[$key] = $value;
        }

        // Restrict certain values
        if ($this->captcha_config['min_length'] < 1) $this->captcha_config['min_length'] = 1;
        if ($this->captcha_config['angle_min'] < 0) $this->captcha_config['angle_min'] = 0;
        if ($this->captcha_config['angle_max'] > 10) $this->captcha_config['angle_max'] = 10;
        if ($this->captcha_config['angle_max'] < $this->captcha_config['angle_min']) $this->captcha_config['angle_max'] = $this->captcha_config['angle_min'];
        if ($this->captcha_config['min_font_size'] < 10) $this->captcha_config['min_font_size'] = 10;
        if ($this->captcha_config['max_font_size'] < $this->captcha_config['min_font_size']) $this->captcha_config['max_font_size'] = $this->captcha_config['min_font_size'];


    }

    /*
     * Generates a new captcha code, and a new capthca image based on the code. The new code is stored in the current session as xs_captcha_code
     * Returns: array(code, imdata_png_base64) (no temp files are created)
    */
    public function generateImage()
    {
        // Use milliseconds instead of seconds
        srand(microtime() * 100);
        // Generate CAPTCHA code if not set by user
        if (empty($this->captcha_config['code'])) {
            $this->captcha_config['code'] = '';
            $length = rand($this->captcha_config['min_length'], $this->captcha_config['max_length']);
            while (strlen($this->captcha_config['code']) < $length) {
                $this->captcha_config['code'] .= substr($this->captcha_config['characters'], rand() % (strlen($this->captcha_config['characters'])), 1);
            }
            //Set the code in the session variable (key: xs_captcha_code)
            $this->session->set('xs_captcha_code', $this->captcha_config['code']);
        }

        // Pick random background, get info, and start captcha
        $background = $this->captcha_config['backgrounds'][rand(0, count($this->captcha_config['backgrounds']) - 1)];
        list($bg_width, $bg_height, $bg_type, $bg_attr) = getimagesize($background);

        $captcha = imagecreatefrompng($background);

        $color = $this->hex2rgb($this->captcha_config['color']);
        $color = imagecolorallocate($captcha, $color['r'], $color['g'], $color['b']);

        // Determine text angle
        $angle = rand($this->captcha_config['angle_min'], $this->captcha_config['angle_max']) * (rand(0, 1) == 1 ? -1 : 1);

        // Select font randomly
        $font = $this->captcha_config['fonts'][rand(0, count($this->captcha_config['fonts']) - 1)];

        // Verify font file exists
        if (!file_exists($font)) throw new Exception('Font file not found: ' . $font);

        //Set the font size.
        $font_size = rand($this->captcha_config['min_font_size'], $this->captcha_config['max_font_size']);
        $text_box_size = imagettfbbox($font_size, $angle, $font, $this->captcha_config['code']);

        // Determine text position
        $box_width = abs($text_box_size[6] - $text_box_size[2]);
        $box_height = abs($text_box_size[5] - $text_box_size[1]);
        $text_pos_x_min = 0;
        $text_pos_x_max = ($bg_width) - ($box_width);
        $text_pos_x = rand($text_pos_x_min, $text_pos_x_max);
        $text_pos_y_min = $box_height;
        $text_pos_y_max = ($bg_height) - ($box_height / 2);
        $text_pos_y = rand($text_pos_y_min, $text_pos_y_max);

        // Draw shadow
        if ($this->captcha_config['shadow']) {
            $shadow_color = $this->hex2rgb($this->captcha_config['shadow_color']);
            $shadow_color = imagecolorallocate($captcha, $shadow_color['r'], $shadow_color['g'], $shadow_color['b']);
            imagettftext($captcha, $font_size, $angle, $text_pos_x + $this->captcha_config['shadow_offset_x'], $text_pos_y + $this->captcha_config['shadow_offset_y'], $shadow_color, $font, $this->captcha_config['code']);
        }

        // Draw text
        imagettftext($captcha, $font_size, $angle, $text_pos_x, $text_pos_y, $color, $font, $this->captcha_config['code']);

        //Output the image in the buffer and then capture it
        ob_start();
        imagepng($captcha);
        $imagedata = ob_get_contents();
        ob_end_clean();
        //\\//

        return array(
            'code' => $this->captcha_config['code'],
            'imdata_png_base64' => base64_encode($imagedata)
        );
    }

    /*
     * Check the $captcha_code against the session stored xs_captcha_code (case insensitive)
     * Returns: boolean (true: code is a match, false: code mismatch)
     */
    public function verifyCode($captcha_code)
    {
        $valid_code = $this->session->get('xs_captcha_code');
        if (strtolower($valid_code) == strtolower($captcha_code)) return true;
        else return false;
    }

    //////////////////////////////////////////////////////////////////////////////
    public function hex2rgb($hex_str, $return_string = false, $separator = ',')
    {
        $hex_str = preg_replace("/[^0-9A-Fa-f]/", '', $hex_str); // Gets a proper hex string
        $rgb_array = array();
        if (strlen($hex_str) == 6) {
            $color_val = hexdec($hex_str);
            $rgb_array['r'] = 0xFF & ($color_val >> 0x10);
            $rgb_array['g'] = 0xFF & ($color_val >> 0x8);
            $rgb_array['b'] = 0xFF & $color_val;
        } elseif (strlen($hex_str) == 3) {
            $rgb_array['r'] = hexdec(str_repeat(substr($hex_str, 0, 1), 2));
            $rgb_array['g'] = hexdec(str_repeat(substr($hex_str, 1, 1), 2));
            $rgb_array['b'] = hexdec(str_repeat(substr($hex_str, 2, 1), 2));
        } else {
            return false;
        }
        return $return_string ? implode($separator, $rgb_array) : $rgb_array;
    }
}
