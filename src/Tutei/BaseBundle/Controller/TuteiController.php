<?php

namespace Tutei\BaseBundle\Controller;

use eZ\Publish\Core\MVC\Symfony\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Tutei\BaseBundle\Classes\Components\Search;

class TuteiController extends Controller
{

    /**
     * Renders in a given component
     *
     * @param string $name
     * @param array $parameters
     * @param string $nameSpace
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showComponent($name, $parameters = array(), $nameSpace = "Tutei\BaseBundle\Classes\Components")
    {
        $class = $nameSpace . "\\" . $name;

        if (class_exists($class)) {

            $component = new $class($this, $parameters);

            if (is_subclass_of($component, "Tutei\\BaseBundle\\Classes\\Components\\Component")) {
                return $component->render();
            }
        }

        return new Response("<h1>Error: Component not found</h1>");
    }

    /**
     * Shows the search results
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction()
    {
        $search = new Search($this);
        return $search->render();
    }

    /**
     * Gets the controller's container
     *
     * @return \Symfony\Component\DependencyInjection\Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    //XPL_PATCH load mail counter
//////////////////////////////
    public function lmcAction()
    {
        $TotalDailyCount = $this->getLMC();
        $response = new Response(json_encode($TotalDailyCount));
        return $response;
    }

//////////////////////////////
    public function lmcHtmlAction()
    {
        $TotalDailyCount = $this->getLMC();
        $TotalDailyCountDots = number_format($TotalDailyCount, 0, ',', '.');
        $TotalDailyCountArr = $this->stringToArray($TotalDailyCountDots);
        $html_output = "";
        foreach ($TotalDailyCountArr as $curChar) {
            if ($curChar == '.') $html_output .= '<span style="background: none"> </span>';
            else $html_output .= '<span>' . $curChar . '</span>';
        }
        $response = new Response($html_output);
        return $response;

    }

//////////////////////////////
    public function getLMC()
    {
        //$to_encode = 'bibi';

        $TotalDailyCount = -1;

        /* //For now, there is no local mail DB
        // Get DAILY(current day) mail count from mail database (local mysql)
        $connection = @mysql_connect( "localhost", "ezpublish", "publish" );


        $ret = @mysql_select_db( "mail", $connection );
        if ( $ret )
        {
            //select SUM() just in case there are more rows with the same day, and they should be cumulated
            $result = mysql_query('SELECT SUM(count) FROM `dailyCount` WHERE DATE(day) = CURDATE()');
            if(mysql_num_rows($result) == 1){
            $dailyCount = (int) mysql_result($result, 0);
            if($dailyCount > 0)$TotalDailyCount += $dailyCount;
            }
        }
        mysql_close($connection);
        */

        // Get DAILY(current day) mail count from mail database (remote on dashboard - 93.90.117.17)
        $connection = @mysql_connect("93.90.117.17", "ezpublish", "hardash3ll");
        if ($connection) {
            $ret = @mysql_select_db("statistics_core", $connection);
            if ($ret) {
                //select SUM() just in case there are more rows with the same day, and they should be cumulated
                $result = mysql_query('SELECT item_value FROM `statistics` WHERE item_name = "global_today_count"');
                if (mysql_num_rows($result) == 1) {
                    $dailyCount = (int)mysql_result($result, 0);
                    if ($dailyCount > 0) $TotalDailyCount += $dailyCount;
                }
            }
            mysql_close($connection);
        }
        if ($TotalDailyCount > 0) $TotalDailyCount++;
        return $TotalDailyCount;
    }

//////////////////////////////
    function stringToArray($s)
    {
        $r = array();
        for ($i = 0; $i < strlen($s); $i++)
            $r[$i] = $s[$i];
        return $r;
    }
    //\\//

}
