<?php

namespace Tutei\BaseBundle\Classes\Components;

use eZ\Publish\API\Repository\Values\Content\Query;
use Symfony\Component\HttpFoundation\Response;

/**
 * Renders page Modals
 */
class Footer extends Component
{
    /**
     * {@inheritDoc}
     */
    public function render()
    {

        $contentService = $this->controller->getRepository()->getContentService();
        $footerContent = $contentService->loadContent(1100);

        $response = new Response();

        $response->setPublic();
        $response->setSharedMaxAge(86400);
        return $this->controller->render(
            'TuteiBaseBundle:parts:footer.html.twig', [
            'footerContent' => $footerContent
        ], $response
        );
    }
}