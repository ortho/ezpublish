<?php /* #?ini charset="utf-8"?
#XPL_MOD

[DatabaseSettings]
DatabaseImplementation=ezmysqli
Server=localhost
Port=
User=newez5
Password=newez5
Database=newez5
Charset=
Socket=disabled

[FileSettings]
VarDir=var

[ExtensionSettings]
ActiveExtensions[]=ezjscore
ActiveExtensions[]=ezoe
ActiveExtensions[]=ggsysinfo
ActiveExtensions[]=ezxmlexport
ActiveExtensions[]=ggsysinfo
ActiveExtensions[]=ezxmlexport
ActiveExtensions[]=ezformtoken
ActiveExtensions[]=ezcommentsbundle
ActiveExtensions[]=ngsymfonytools
ActiveExtensions[]=eztags

[Session]
SessionNameHandler=custom

[SiteSettings]
SiteName=SMTP.dk
SiteURL=ezp5
DefaultAccess=site
SiteList[]=site
SiteList[]=siteadmin
#XPL_PATCH SSL port for siteadmin
SSLPort=17444

[SiteAccessSettings]
CheckValidity=false
AvailableSiteAccessList[]=site
AvailableSiteAccessList[]=siteadmin
RelatedSiteAccessList[]=site
RelatedSiteAccessList[]=siteadmin
MatchOrder=uri
HostMatchMapItems[]

[DesignSettings]
DesignLocationCache=enabled

[MailSettings]
Transport=sendmail
AdminEmail=nospam@nospam.com
EmailSender=
*/ ?>