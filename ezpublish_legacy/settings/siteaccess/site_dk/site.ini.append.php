<?php /* #?ini charset="utf-8"?

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
LoginPage=embedded

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
ShowHiddenNodes=false

[DesignSettings]
SiteDesign=plain_site
AdditionalSiteDesignList[]=base

[RegionalSettings]
Locale=dan-DK
ContentObjectLocale=dan-DK
SiteLanguageList[]=dan-DK
TextTranslation=enabled

[FileSettings]
VarDir=var

[ContentSettings]
TranslationList=

[MailSettings]
AdminEmail=nospam@nospam.com
EmailSender=
*/ ?>
