<?php /* #?ini charset="utf-8"?
# XPL_mod #
# eZ publish configuration file for content and ez xml tags
#

# Some custom tags add special features to the editor if enabled:
# underline: adds underline button in edtor (use this instead of custom tag to make the text appear underlined)
# pagebreak: adds a button to add pagebreaks
# NOTE: view template is not included for pagebreak tag, so you need to implement it to see it
#
#[CustomTagSettings]
#AvailableCustomTags[]=pagebreak
#AvailableCustomTags[]=underline
#IsInline[underline]=true
#AvailableCustomTags[]=span
## Displays the custom tag as an image so you cannot create sub content.
## Will use custom image if there is a custom attribute on the tag named 'image_url'
#IsInline[externalimage]=image
## Lets you specify 22x22 icon to use on custom image tag if it doesn't have 'image_url' attribute
#InlineImageIconPath[mashup]=images/tango/image-x-generic22.png


[paragraph]
# Human-readable aliases for class names that will be displayed
# in the "Class" dropdowns of dialog windows.
#ClassDescription[pRed]=Red bar
AvailableCustomTags[]=[span]

[link]
AvailableViewModes[]=full
AvailableViewModes[]=line
AvailableClasses[]=btn btn-lg btn-primary
AvailableClasses[]=btn btn-lg btn-warning
AvailableClasses[]=showSingle
AvailableClasses[]=showSingle dashboard-sidelink

[table]
Defaults[rows]=2
Defaults[cols]=2
Defaults[width]=100%
Defaults[border]=0
#Defaults[class]=myclass
AvailableClasses[]=table
AvailableClasses[]=buiaka2
AvailableClasses[]=buiaka3

[td]
AvailableClasses[]=readmore
AvailableClasses[]=xpl_infos
AvailableClasses[]=td_title
AvailableClasses[]=td_header
AvailableClasses[]=td_name
AvailableClasses[]=td_content


[tr]
AvailableClasses[]=white
AvailableClasses[]=alpha30
AvailableClasses[]=alpha40
AvailableClasses[]=alpha60

[paragraph] 
AvailableClasses[]=toggle-overflow
AvailableClasses[]=toggle-select-me
AvailableClasses[]=kontaktboxes1
AvailableClasses[]=kontaktboxes2
AvailableClasses[]=kontaktboxes3


[strong]
AvailableClasses[]=xpl_semibold

[embed]
AvailableViewModes[]=embed
AvailableViewModes[]=embed-inline
AvailableViewModes[]=full
AvailableViewModes[]=line

[ul]
AvailableClasses[]=flipflopul

[li]
AvailableClasses[]=dashboard
AvailableClasses[]=email
AvailableClasses[]=rapportering
AvailableClasses[]=profil
AvailableClasses[]=abonnement

[embed-type_images]
AvailableClasses[]=img-responsive
AvailableClasses[]=grayscale
AvailableClasses[]=trustpilot-img

[embed-inline]
AvailableViewModes[]=embed-inline
AvailableClasses[]=grayscale
AvailableClasses[]=trustpilot-img

# Extra ezoe settings for embed and embed-inline.
# If you want to limit the amount of AvailableClasses and/or CustomAttributes
# on relations per class identifier or content type* you can do the following:
# NB: These settings also needs to be defined in [embed] or [embed-inline] for eZ Publish.
#
# Pattern for content type:
#[<tag>-type_<content-type>]
#
## This examples demonstrates limiting AvailableClasses list in editor
## when editing embed tags with a relation to a object of content type image.
#[embed-type_images]
#AvailableClasses[]
#AvailableClasses[]=blue_border
#AvailableClasses[]=dropp_down_shadow
#
# Pattern for class identifier:
#[<tag>_<class_identifier>]
#
## This example removes class list on embed-inline flash objects
#[embed-inline_flash]
#AvailableClasses[]
#
# *content type as defined by content.ini [RelationGroupSettings]

*/ ?>