use v5.12;
use JSON;
use Data::RandomPerson;
use LWP::UserAgent;
use HTTP::Request::Common qw(POST);
use File::Slurp qw( read_file );
use Data::Dumper;
use Data::Dump;

## Check 
# http://xmodulo.com/2013/05/how-to-send-http-get-or-post-request-in-perl.html
# http://www.clubtexting.com/sms-gateway/rest-api-samples/perl.html

## SSL warnings suck ass
$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;

my $ua = LWP::UserAgent->new;
# SOCK 4 stuff  
#$ua->proxy([qw(http https)] => 'socks4://170.0.2.172:1080');
#$ua->proxy([qw(http https)] => 'socks4://170.0.2.172:1080');

#my $server_endpoint = "https://170.0.2.172/xsmtpdk/api/test";
# my $server_endpoint = "https://170.0.2.173/xsmtpdk/api/copy_subscription";


my $server_endpoint = "https://170.0.2.174/xsmtpdk/api/goodness";


# set custom HTTP request header fields
#my $req = HTTP::Request->new(GET => $server_endpoint);
my $req = HTTP::Request->new(POST => $server_endpoint);
## Very important
#$req->header('content-type' => 'application/x-www-form-urlencoded');
$req->header('content-type' => 'application/json');
#$req->header('x-auth-token' => 'kfksj48sdfj4jd9d');
 
# add POST data to HTTP request body
#my $post_data = '{ "name": "Dan", "address": "NY" }';

# first try with nodeIDs
# and this user https://170.0.2.173/siteadmin/Users/accounts_english/Account-ceva121-ceva.com-Name-first1-last/Subscriptions
# $xsSubsc->copySubscription(249, 1497);


# base_shared SUIDs are stuff like basic1, premium_special, family1, gratis_test

my $subscriptions = 'subscriptions.txt';

my $line = read_file( $subscriptions ); 

chomp($line);

foreach my $l (split("\n", $line ) ) {

#	say $l;
	
	my ($type, $suid, $login) = split(";", $l );

	say Dumper $login; 

	my %rec_hash = ('login'=>$login, 'suid'=>$suid, 'type'=>$type);

	my $json = encode_json \%rec_hash;

	say Dumper %rec_hash;

	#my $post_data = qx/{"email":"cucu11090611125721\@example.com","login":"cucu10191110625721\@example.com","password":"tester123","first_name":"$first","last_name":"$last","postal_code":"061234","address":"Emirates Holidays Building,\n Sheikh Zayed Road\n P.O Box 7631","city":"Deva","company":"Boston Dynamics","country":"United Arab Emirates","creation_date":"2014-07-31 16:25:38","telephone":"+971 4 214 4888","lang":"eng-GB"}/;

	#$req->content("a:".$post_data);

	#$req->content("userdata=".$json);
	$req->content($json);
	 
	my $resp = $ua->request($req);


	if ($resp->is_success) {
	    my $message = $resp->decoded_content;
	    say "Received reply: $message";
	}
	else {
	   my $message = $resp->decoded_content;

	    say "HTTP POST error code: ", $resp->code;
	    say "HTTP POST error message: ", $resp->message;
	    say "Received reply: $message";

	}

}

