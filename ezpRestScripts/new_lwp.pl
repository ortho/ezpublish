use v5.12;
use JSON;
use Data::RandomPerson;
use LWP::UserAgent;
use HTTP::Request::Common qw(POST);
use File::Slurp qw( read_file );
use Data::Dumper;

use Data::RandomPerson;
use Data::RandomPerson::Names::Male;
use Data::RandomPerson::Names::VikingFemale;


## Check 
# http://xmodulo.com/2013/05/how-to-send-http-get-or-post-request-in-perl.html
# http://www.clubtexting.com/sms-gateway/rest-api-samples/perl.html

## SSL warnings suck ass
$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;


sub run {
#    @_ or die "Need path to word file\n";
    my $filename = "/root/names.txt";

    open my $words_fh, '<', $filename
        or die "Cannot open word file '$filename': $!";

    my $words = read_file(
        $words_fh,
        array_ref => 1,
        'chomp' => 1,
        blk_size => 4_000_000,
    );

    close $words_fh
        or die "Cannot close word file '$filename': $!";

    say join('', lc pick($words));
    return;
}

sub pick {
    my $set = shift;

    my $j = int rand(@$set);
    my $k = int rand(@$set - 1);

    $k += ($k >= $j);

#    return @{ $set }[$j, $k];
    return @{ $set }[$k];
}


our @frequentv = split " ", "
    Nagy Kov\xe1cs T\xf3th Szab\xf3 Horv\xe1th Varga Kiss Moln\xe1r
    N\xe9meth Farkas Balogh Papp Tak\xe1cs Juh\xe1sz Lakatos
    M\xe9sz\xe1ros Simon Ol\xe1h Fekete R\xe1cz Szil\xe1gyi
    T\xf6r\xf6k Feh\xe9r G\xe1l Bal\xe1zs Pint\xe9r Sz\x{171}cs
    Kocsis Fodor Kis Szalai Magyar Sipos Ors\xf3s Luk\xe1cs Bir\xf3
    Guly\xe1s Kir\xe1ly Katona L\xe1szl\xf3 Fazekas S\xe1ndor Boros
    Jakab Kelemen Somogyi Antal Vincze Heged\x{171}s F\xfcl\xf6p
    Orosz Bogd\xe1n Veres De\xe1k V\xe1radi Balog Budai B\xe1lint
    Sz\x{151}ke Pap Bogn\xe1r Vass V\xf6r\xf6s P\xe1l Ill\xe9s
    Sz\xfccs Lengyel F\xe1bi\xe1n Bodn\xe1r Hal\xe1sz Hajdu
    G\xe1sp\xe1r Kozma P\xe1sztor Bakos Sz\xe9kely Major Dud\xe1s
    Nov\xe1k Heged\xfcs J\xf3n\xe1s M\xe1t\xe9 Orb\xe1n So\xf3s
    Vir\xe1g Barna Nemes Pataki Szekeres Tam\xe1s Farag\xf3 Borb\xe9ly
    Balla Barta V\xe9gh Kerekes Dobos Kun P\xe9ter Csonka
";
our @frequentf = split " ", "
    L\xe1szl\xf3 Istv\xe1n J\xf3zsef J\xe1nos Zolt\xe1n S\xe1ndor
    Ferenc G\xe1bor Attila P\xe9ter Tam\xe1s Tibor Zsolt Imre Lajos
    Andr\xe1s Gy\xf6rgy Csaba Gyula Mih\xe1ly K\xe1roly B\xe9la
    Bal\xe1zs Mikl\xf3s R\xf3bert P\xe1l Kriszti\xe1n D\xe1vid
    Norbert D\xe1niel \xc1d\xe1m Antal Szabolcs Bence G\xe9za
    Roland M\xe1t\xe9 Rich\xe1rd Gergely \xc1rp\xe1d Gerg\x{151}
    B\xe1lint Viktor M\xe1rk \xc1kos Jen\x{151} K\xe1lm\xe1n M\xe1rton
    Ern\x{151} Levente Dezs\x{151} Endre M\xe1ty\xe1s Krist\xf3f
    Patrik Barnab\xe1s Martin N\xe1ndor Vilmos Ott\xf3 Szil\xe1rd
    D\xe9nes Bertalan Mil\xe1n Marcell Erik Dominik Rudolf Alex
    Korn\xe9l Albert \xc1ron Oliv\xe9r Gy\x{151}z\x{151} Zsigmond
    Guszt\xe1v Ervin Vince Elem\xe9r Adri\xe1n Benj\xe1min Andor
    Szilveszter Iv\xe1n Benedek Botond Tivadar Zsombor Emil Barna
    Henrik Arnold Elek Rezs\x{151} Kevin L\xf3r\xe1nt Ign\xe1c
    M\xe1ri\xf3 Alad\xe1r Frigyes
";
our @frequentn = split " ", "
    M\xe1ria Erzs\xe9bet Ilona Katalin \xc9va Anna Margit Zsuzsanna
    Julianna Judit \xc1gnes Ir\xe9n Andrea Ildik\xf3 Erika Krisztina
    Magdolna Eszter Edit Roz\xe1lia M\xf3nika Gabriella Szilvia
    Piroska M\xe1rta Anita Anik\xf3 Kl\xe1ra Gizella Ibolya T\xedmea
    Vikt\xf3ria Ter\xe9zia T\xfcnde Veronika Jol\xe1n Zs\xf3fia
    Csilla D\xf3ra Alexandra Etelka Marianna Melinda Be\xe1ta Ter\xe9z
    Nikolett Adrienn Ren\xe1ta Rita Gy\xf6ngyi Borb\xe1la Bernadett
    Brigitta Hajnalka Edina Val\xe9ria Barbara Enik\x{151} Orsolya
    R\xf3za R\xe9ka N\xf3ra Aranka Vivien Annam\xe1ria Nikoletta Irma
    Petra No\xe9mi R\xf3zsa Kitti Anett Emese Klaudia Beatrix Fanni
    Bogl\xe1rka Zita Zsanett Kinga Gy\xf6rgyi Lilla Olga Sarolta
    J\xfalia Ida Mariann Henrietta Laura Emma Di\xe1na S\xe1ra
    Bettina Szabina Ang\xe9la Dorottya Evelin L\xedvia Bianka Dorina
";
sub randname {
    my $r = $frequentv[rand@frequentv];
    my $u = rand() < 0.5 ? \@frequentf : \@frequentn;
    my $i = rand@$u;
    $r .= " " . $$u[$i];
    if (rand() < 0.15) {
        my $j = rand@$u;
        if ($i != $j) {
            $r .= " " . $$u[$j];
        }
    }
    $r;
}


binmode STDOUT, "encoding(utf-8)";

#say randname; 


my $filename = "tlds.txt";

my @tlds = read_file($filename);

## If the array is full of 1s
## http://stackoverflow.com/questions/1458454/why-is-the-list-my-perl-map-returns-just-1s

my @tld_array = map {
 chomp (my $foo = $_);
    $foo;
} @tlds;



my $country_filename = "countries.txt"; 

my @countries = read_file($country_filename);


my @country_array = map {
 chomp (my $foo = $_);
    $foo;
} @countries;


#say Dumper @country_array;

my ($first, $last) = split( / /, randname); 

#say $first; 
#say $last; 


my $n = Data::RandomPerson::Names::VikingFemale->new();
my $random_email = lc($n->get());

my $m = Data::RandomPerson::Names::Male->new();
my $random_domain_name =  lc($m->get());

my $random_tld = $tld_array[rand @tld_array];

my $email = $random_email."@".$random_domain_name.".".$random_tld;
#say $email;


my $ua = LWP::UserAgent->new;
# SOCK 4 stuff  
#$ua->proxy([qw(http https)] => 'socks4://170.0.2.172:1080');
#$ua->proxy([qw(http https)] => 'socks4://170.0.2.172:1080');

#my $server_endpoint = "https://170.0.2.172/xsmtpdk/api/test";
my $server_endpoint = "https://170.0.2.174/xsmtpdk/api/createuser";


# set custom HTTP request header fields
#my $req = HTTP::Request->new(GET => $server_endpoint);
my $req = HTTP::Request->new(POST => $server_endpoint);
## Very important
#$req->header('content-type' => 'application/x-www-form-urlencoded');
$req->header('content-type' => 'application/json');
#$req->header('x-auth-token' => 'kfksj48sdfj4jd9d');
 
# add POST data to HTTP request body
#my $post_data = '{ "name": "Dan", "address": "NY" }';

my $first_name = $first;
my $last_name = $last;
#my $email = ""; 
#my $login = ""; 
my $password = "2secret4u";  
#my $first_name = $first; 
#my $last_name = $last; 
my $postal_code = "672349867";
my $address = "Something Street"; 
my $city = "Abu Dhabi"; 
my $company = "ACME"; 
my $creation_date = "2014-07-31 16:25:38"; 
my $telephone = "673496754"; 
my $lang = "eng-GB"; 

#my $temp_email = "thora\@courtney.email";

my %rec_hash = ('email'=>$email, 'login'=>$email,'password'=>$password,'first_name'=>$first_name, 'last_name'=>$last_name, 'postal_code'=>$postal_code, 'address'=>$address, 'city'=>$city, 'company'=>$company, 
'creation_date'=>$creation_date, 'telephone'=>$telephone, 'lang'=>$lang  );

my %new_hash = ('userdata' => %rec_hash); 

my $json = encode_json \%rec_hash;


say Dumper %rec_hash;

#my $post_data = qx/{"email":"cucu11090611125721\@example.com","login":"cucu10191110625721\@example.com","password":"tester123","first_name":"$first","last_name":"$last","postal_code":"061234","address":"Emirates Holidays Building,\n Sheikh Zayed Road\n P.O Box 7631","city":"Deva","company":"Boston Dynamics","country":"United Arab Emirates","creation_date":"2014-07-31 16:25:38","telephone":"+971 4 214 4888","lang":"eng-GB"}/;

#$req->content("a:".$post_data);

#$req->content("userdata=".$json);
$req->content("{\"userdata\":".$json."}");
 
my $resp = $ua->request($req);


if ($resp->is_success) {
    my $message = $resp->decoded_content;
    print "Received reply: $message\n";
}
else {
    print "HTTP POST error code: ", $resp->code, "\n";
    print "HTTP POST error message: ", $resp->message, "\n";
}
