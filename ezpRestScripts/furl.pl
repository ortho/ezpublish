use Furl;

my $furl = Furl->new(
        agent   => 'MyGreatUA/2.0',
        timeout => 10,
 );

my $res = $furl->get('http://example.com/');
die $res->status_line unless $res->is_success;
print $res->content;

my $res = $furl->post(
        'http://example.com/', # URL
        [...],                 # headers
        [ foo => 'bar' ],      # form data (HashRef/FileHandle are also okay)
);

# Accept-Encoding is supported but optional
$furl = Furl->new(
        headers => [ 'Accept-Encoding' => 'gzip' ],
    );

my $body = $furl->get('http://example.com/some/compressed');
