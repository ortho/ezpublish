source /root/rest.cfg

source ez_repo.sh

# The generation
echo '{
	"UserCreate": {
		"mainLanguageCode": "eng-GB",
		"Section": {
		"_href": "/api/ezp/v2/content/sections/2"
	},
	"remoteId": "remote'$remoteid'",
	"login": "'$login'",
	"email": "'$login'@'$domain'.'$tld'",
	"password": "2secr3t",
	"enabled": "true",
	"fields": {
		"field": [
				{
					"fieldDefinitionIdentifier": "first_name",
					"languageCode": "eng-GB",
					"fieldValue": "'$name'"
			},
			{
				"fieldDefinitionIdentifier": "last_name",
				"languageCode": "eng-GB",
				"fieldValue": "'$surname'"
			}
			]
		}
	}
}' | \
http --auth $username:$password -v POST $host/api/ezp/v2/user/groups/1/5/$english/users \
'Accept:application/vnd.ez.api.User+json' \
'Content-Type:application/vnd.ez.api.UserCreate+json'  --verify=no  # don't verify the https:// cert 

