source /root/rest.cfg

source ez_repo.sh

#mumu=$(( ( RANDOM % 1000000 )  + 1000 ))
#hash="$(echo -n "$1$mumu" | md5sum )"
#echo "$hash"

#login=`consonant; vowel; vowel; consonant; vowel; consonant; vowel; consonant`

# Node ID: 255 - english 
# Node ID: 280 - danish  

# "_href": "/api/ezp/v2/content/locations/1/5/255/262",

# it will showup here
# https://170.0.2.173/siteadmin/Users/accounts_english

login=`gen_random_username`
name=`name`
surname=`name`

domain=`vowel; vowel; consonant; vowel; consonant; vowel; consonant; consonant`
tld=`vowel; consonant; vowel`
remoteid=`randomString32`

danish=280
english=255

# The generation
<<MAH
echo '{
	"UserCreate": {
		"mainLanguageCode": "eng-GB",
		"Section": {
		"_href": "/api/ezp/v2/content/sections/2"
	},
	"remoteId": "remote'$remoteid'",
	"login": "'$login'",
	"email": "'$login'@'$domain'.'$tld'",
	"password": "2secr3t",
	"enabled": "true",
	"fields": {
		"field": [
				{
					"fieldDefinitionIdentifier": "first_name",
					"languageCode": "eng-GB",
					"fieldValue": "'$name'"
			},
			{
				"fieldDefinitionIdentifier": "last_name",
				"languageCode": "eng-GB",
				"fieldValue": "'$surname'"
			}
			]
		}
	}
}' | \
http --auth $username:$password -v POST $host/api/ezp/v2/user/groups/1/5/$english/users \
'Accept:application/vnd.ez.api.User+json' \
'Content-Type:application/vnd.ez.api.UserCreate+json'  --verify=no  # don't verify the https:// cert 
MAH


#echo '{"email":"'$login'@'$domain'.'$tld'","login":"'$login'","password":"tester123","first_name":"'$name'","last_name":"'$surname'","postal_code":"061234","address":"Emirates Holidays Building,\n Sheikh Zayed Road\n P.O Box 7631","city":"Deva","company":"Boston Dynamics","country":"United Arab Emirates","creation_date":"2014-07-31 16:25:38","telephone":"+971 4 214 4888","lang":"eng-GB"}  ' | \

cacat='{"email":"'$login'@'$domain'.'$tld'","login":"'$login'","password":"tester123","first_name":"'$name'","last_name":"'$surname'","postal_code":"061234","address":"Emirates Holidays Building,\n Sheikh Zayed Road\n P.O Box 7631","city":"Deva","company":"Boston Dynamics","country":"United Arab Emirates","creation_date":"2014-07-31 16:25:38","telephone":"+971 4 214 4888","lang":"eng-GB"}' 

#echo $cacat; 

#echo "userdata:$cacat"; 

#curl --proxy socks5h://localhost:1080 -X POST   "userdata:$cacat"  https://beta.mysmtp.eu/xsmtpdk/api/createuser -k


curl --proxy socks5h://localhost:1080 -X POST  --data  "userdata:'{"email":"'$login'@'$domain'.'$tld'","login":"'$login'","password":"tester123","first_name":"'$name'","last_name":"'$surname'","postal_code":"061234","address":"Emirates Holidays Building,\n Sheikh Zayed Road\n P.O Box 7631","city":"Deva","company":"Boston Dynamics","country":"United Arab Emirates","creation_date":"2014-07-31 16:25:38","telephone":"+971 4 214 4888","lang":"eng-GB"}'"


#http -v POST https://beta.mysmtp.eu/xsmtpdk/api/createuser --verify=no
