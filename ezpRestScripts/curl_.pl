#use strict;
use warnings;
use WWW::Curl::Easy;


my  $curl  = WWW::Curl::Easy->new( check_ssl_certs => 0  );


my $host = 'localhost';
my $port = '1080';

my $timeout = '20';
my $connectTimeOut= '20';

#setejem el proxy
$curl->setopt(CURLOPT_PROXY,"$host:".$port);
$curl->setopt(CURLOPT_PROXYTYPE,CURLPROXY_SOCKS4);

#$curl->setopt(CURLOPT_SSL_VERIFYPEER, 0);
#posem les altres dades
#$curl->setopt(CURLOPT_USERAGENT, $useragent);
$curl->setopt(CURLOPT_VERBOSE,1);
$curl->setopt(CURLOPT_CONNECTTIMEOUT, $connectTimeOut);
$curl->setopt(CURLOPT_TIMEOUT, $timeout);
$curl->setopt(CURLOPT_SSL_VERIFYPEER,0);
$curl->setopt(CURLOPT_HEADER,0);



        
$curl->setopt(CURLOPT_HEADER,1);
$curl->setopt(CURLOPT_URL, 'https://beta.mysmtp.eu');

# A filehandle, reference to a scalar or reference to a typeglob can be used here.
my $response_body;
$curl->setopt(CURLOPT_WRITEDATA,\$response_body);

# Starts the actual request
my $retcode = $curl->perform;

# Looking at the results...
if ($retcode == 0) {
                print("Transfer went ok\n");
                my $response_code = $curl->getinfo(CURLINFO_HTTP_CODE);
                # judge result and next action based on $response_code
                print("Received response: $response_body\n");
} else {
                # Error code, type of error, error message
                print("An error happened: $retcode ".$curl->strerror($retcode)." ".$curl->errbuf."\n");
}
