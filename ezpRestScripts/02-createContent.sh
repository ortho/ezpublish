source /root/rest.cfg

echo '{
  "ContentCreate": {
    "ContentType": {
      "_href": "/api/ezp/v2/content/types/13"
    },
    "mainLanguageCode": "eng-GB",
    "LocationCreate": {
      "ParentLocation": {
        "_href": "/api/ezp/v2/content/locations/1/5"
      },
      "priority": "0",
      "hidden": "false",
      "sortField": "PATH",
      "sortOrder": "ASC"
    },
    "Section": {
      "_href": "/api/ezp/v2/content/sections/4"
    },
    "alwaysAvailable": "true",
    "remoteId": "remoteId1234567899",
    "User": {
      "_href": "/api/ezp/v2/user/users/14"
    },
    "modificationDate": "2012-09-30T12:30:00",
    "fields": {
      "field": [
        {
          "fieldDefinitionIdentifier": "subject",
          "languageCode": "eng-GB",
          "fieldValue": "Test comment"
        },
        {
          "fieldDefinitionIdentifier": "author",
          "languageCode": "eng-GB",
          "fieldValue": "Edi Modric"
        },
        {
          "fieldDefinitionIdentifier": "message",
          "languageCode": "eng-GB",
          "fieldValue": "This is a comment"
        }
      ]
    }
  }
}' | \
http --auth $username:$password -v POST $host/api/ezp/v2/content/objects?publish=true \
'Accept:application/vnd.ez.api.Content+json' \
'Content-Type:application/vnd.ez.api.ContentCreate+json' --verify=no  # don't verify the https:// cert 

